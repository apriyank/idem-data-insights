==================
idem-data-insights
==================

.. image:: https://img.shields.io/badge/made%20with-pop-teal
   :alt: Made with pop, a Python implementation of Plugin Oriented Programming
   :target: https://pop.readthedocs.io/

.. image:: https://img.shields.io/badge/made%20with-python-yellow
   :alt: Made with Python
   :target: https://www.python.org/

Description
===========

This project is for gathering insights of SLS data.

Run following commands::

    pip install -e .
    idem_data_insights -s [path_to_sls_file]


Run help command to understand more about configuration parameters::

    idem_data_insights --help


More details will be added soon.
